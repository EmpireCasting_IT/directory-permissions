﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.DirectoryServices.AccountManagement;

namespace Directory_Permissions
{
    public class PermissionClass
    {
        public static void ChangePermissions(string[] adminUsers, string[] restrictedUsers, string permissions, string[] removeUsers, string folder)
        {
            string[] myDirs = Directory.GetDirectories(folder, "*", SearchOption.AllDirectories);
            
            Console.WriteLine("");
            Console.WriteLine("Directories:");
            Console.WriteLine(folder);
            
            UserPermissions(adminUsers, null, null, removeUsers, folder);

            foreach (var myDir in myDirs)
            {
                Console.WriteLine(myDir);
                
                if (myDir.Substring(myDir.LastIndexOf("\\")).Contains("."))
                {
                    string[] admins = { "ECC\\" + myDir.Substring(myDir.LastIndexOf("\\") + 1) };

                    UserPermissions(admins, restrictedUsers, permissions, removeUsers, myDir);
                }
            }
        }

        private static void UserPermissions(string[] adminUsers, [Optional] string[] restrictedUsers, [Optional] string permissions, [Optional] string[] removeUsers, string folder)
        {
            try
            {
                System.IO.DirectoryInfo directoryInfo = new System.IO.DirectoryInfo(folder);
                DirectorySecurity directorySec = directoryInfo.GetAccessControl();


                foreach (string user in adminUsers)
                {
                    try
                    {
                        NTAccount acct = new NTAccount(user);
                        IdentityReference id = acct.Translate(typeof(SecurityIdentifier));
                        FileSystemAccessRule admin_users = null;

                        if (user.ToLower().Contains("admin"))
                        {
                            // Give Admin FullControl and access to all options.
                            admin_users = new FileSystemAccessRule(id, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit
                        | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);
                        }
                        else
                        {
                            // Make user account to have FullControl and "Applies to: subfolders and files only."
                            admin_users = new FileSystemAccessRule(id, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit
                        | InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);
                        }

                        directorySec.AddAccessRule(admin_users);
                        directoryInfo.SetAccessControl(directorySec);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("admin_users:");
                        Console.WriteLine(e.Message);
                    }
                }


                if (!(permissions == null))
                {
                    foreach (string user in restrictedUsers)
                    {
                        NTAccount acct = new NTAccount(user);
                        IdentityReference id = acct.Translate(typeof(SecurityIdentifier));
                        FileSystemAccessRule restricted_users = null;
                        FileSystemAccessRule restricted_users2 = null;

                        switch (permissions)
                        {
                            case "Modify":
                                restricted_users = new FileSystemAccessRule(id, FileSystemRights.Modify, InheritanceFlags.ContainerInherit
                        | InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);

                                restricted_users2 = new FileSystemAccessRule(id, FileSystemRights.DeleteSubdirectoriesAndFiles, InheritanceFlags.ContainerInherit
                        | InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);

                                break;

                            case "ReadAndExecute":
                                restricted_users = new FileSystemAccessRule(id, FileSystemRights.ReadAndExecute, InheritanceFlags.ContainerInherit
                        | InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);

                                break;
                        }

                        if (!(restricted_users == null))
                        {
                            try
                            {
                                directorySec.AddAccessRule(restricted_users);
                                
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("restricted_users:");
                                Console.WriteLine(e.Message);
                            }
                        }
                        if (!(restricted_users2 == null))
                        {
                            try
                            {
                                directorySec.AddAccessRule(restricted_users2);

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("restricted_users2:");
                                Console.WriteLine(e.Message);
                            }
                        }
                        directoryInfo.SetAccessControl(directorySec);
                    }
                }


                if (!(removeUsers == null))
                { 
                    foreach (string user in removeUsers)
                    {
                        //Get the account to remove.
                        NTAccount acct = new NTAccount(user);
                        IdentityReference id = acct.Translate(typeof(SecurityIdentifier));

                        //Remove all access to the directory.
                        FileSystemAccessRule rule = new FileSystemAccessRule(id,
                                FileSystemRights.FullControl, AccessControlType.Allow);
                        directorySec.RemoveAccessRuleAll(rule);

                        //Apply the changes.
                        directoryInfo.SetAccessControl(directorySec);
                    }
                }
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine("");
                Console.WriteLine(e.Message);
            }
        }
    }
}
