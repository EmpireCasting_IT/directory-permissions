﻿using static Directory_Permissions.PermissionClass;
using System;

namespace Directory_Permissions
{
    class Startup
    {
        static void Main()
        {
            string[] adminUsers = null, restricedUsers = null;
            string[] removeUsers = null;
            string permissions = null;
            string folder = null;

            Console.Write("Please enter Admin Users (to give standard users Full Rights, type 'Admin-' before their name. seperate users by a comma): ");
            adminUsers = Console.ReadLine().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            Console.Write("Please enter restricted Users (separated by comma): ");
            restricedUsers = Console.ReadLine().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            Console.Write("Please enter the permissions for restricted user (ie. Modify, ReadAndExecute): ");
            permissions = Console.ReadLine();

            Console.Write("Please enter Users to remove (separated by comma): ");
            removeUsers = Console.ReadLine().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            Console.Write("Please enter the full directory path to change permissions for: ");
            folder = Console.ReadLine();


            ChangePermissions(adminUsers, restricedUsers, permissions, removeUsers, folder);
        }
    }
}
